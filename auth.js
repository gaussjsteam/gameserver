"use strict"
var express = require('express');
var bodyParser = require('body-parser');
var bcrypt = require('bcrypt');
var User = require("./models/User");
var AuthService = require('./services/AuthService');

var secret = 'tEfIRCTzfpG6fNW2BRv2GLXzuK4ivQpzgmHaf3KM';
var router = express.Router();

router.use(bodyParser.json());       // to support JSON-encoded bodies
router.use(bodyParser.urlencoded({     // to support URL-encoded bodies
  extended: true
}));

var hashPassword = function(password, cb) {
  bcrypt.genSalt(10, (err, salt) => {
    bcrypt.hash(password, salt, (err, hash) => {
      cb(hash);
    });
  });
}

router.use('/', function(req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  next();
})

router.post('/register', function(req, res) {
  console.log('registration', req.body);
  var user = {
    username: req.body.username,
    email: req.body.email,
    password: req.body.password
  };
  if(!user.username) return res.json(400, {success: false, message: 'Username is required'});
  if(!user.password) return res.json(400, {success: false, message: 'Password is required'});
  hashPassword(user.password, function(hash) {
    user.password = hash;
    User.create(user, function(err, user) {
      if(err) res.json(400, {success: false, message: err.message});
      var data = {
        id: user._id,
        username: user.username,
        email: user.email
      };
      res.json({success:true, token: AuthService.signToken(data), user: data});
    })
  });
});

router.get('/guestAccess', function(req, res) {
  res.json({token: AuthService.signToken({})});
})

router.post('/generateToken', function(req, res) {
  var username = req.body.username;
  if(!username) return res.json(400, {success: false, message: 'Username is required'});

  User.findOne({username:username}, function(err, user) {
    if(err) return res.send(400, err.message);
    if(!user) {
      User.create({username, username}, function(err, user) {
        var data = {
          id: user._id,
          username: user.username
        };
        res.json({token: AuthService.signToken(data), user: data});
      })
    } else {
      var data = {
        id: user._id,
        username: user.username,
        email: user.email
      };
      res.json({token: AuthService.signToken(data), user: data});
    }
  })
})

router.post('/login', function(req, res) {
  var username = req.body.username;
  var password = req.body.password;

  User.findOne({$or:[{username: username},{email:username}]}, function(err, user) {
    if(err) return res.send(400, err.message);
    if(!user) return res.send(403, 'User not found');
    else {
      bcrypt.compare(password, user.password, function (err, result) {
        if (!result) {
          res.send(403, 'Password does not match');
        } else {
          var data = {
            id: user._id,
            username: user.username,
            email: user.email
          };
          res.json({token: AuthService.signToken(data), user: data});
        }
      });
    }
  });
});

router.post('/reset', function(req, res) {
  res.send('reset password');
});

module.exports = router;
