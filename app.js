"use strict";

var HitMe = require('./games/HitMe');
var Mafia = require('./games/Mafia');
var Player = require('./classes/Player');
var Session = require('./classes/Session');
var AuthService = require('./services/AuthService');

var express = require('express');
var app = express();
var server = require('http').Server(app);
var io = require('socket.io')(server);
var _ = require('lodash');

var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/game');

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
  console.log('db connection established');
  // we're connected!
});

server.listen(1346);

app.use('/', require('./auth.js'));

var onlineUsers = {};
var guestsCount = 0;

var game = new HitMe(io);
var mafia = new Mafia(io, {maxPlayers:12});
var privateSessions = {};

io.on('connection', function (socket) {

    socket.eventHandlers = {};

    var authTimeout = setTimeout(function() {
      socket.disconnect('unauthorized');
    }, 5000);

    socket.once('authenticate', data => {
      try {

        clearTimeout(authTimeout);

        var data = AuthService.verifyToken(data.token);
        var username = data.username;

        if(!username) {
          username = username = 'guest_'+guestsCount;
          guestsCount++;
        }

        if(onlineUsers[username]) {
          socket.disconnect('already connected');
          return;
        }

        socket.emit('authenticated', {
          username: username
        });

        console.log('user connected', username);

        var player = new Player(socket, username);

        socket.on('event', data => {
          console.log(username+ ' event', data)
          var eventName = data.name;
          if(socket.eventHandlers[eventName]) socket.eventHandlers[eventName](data.data);
        })

        socket.emitEvent = (name, data) => {
          socket.emit('event', {name:name, data:data})
        }

        socket.broadcastEvent = (name, data) => {
          socket.broadcast.emit('event', {name:name, data:data});
        }

        socket.emitEvent('onlineUsers', _.keys(onlineUsers));
        socket.broadcastEvent('userConnected', {user:username});

        onlineUsers[username] = player;

        socket.on('disconnect', () => {
          delete onlineUsers[username];
          socket.broadcast.emit('event', {name:'userDisconnected', data:{user:username}});
          console.log('user left', username);
        });

        socket.eventHandlers['lobbyMessage'] = data => {
          socket.broadcastEvent('lobbyMessage', {user:username, message:data});
        }

        socket.eventHandlers['privateMessage'] = data => {
          if(onlineUsers[data.user]) onlineUsers[data.user].socket.emitEvent('privateMessage', {user:username, message:data.message});
        }

        socket.eventHandlers['play'] = () => game.joinQueue(player);

        socket.eventHandlers['leaveQueue'] = () => game.leaveQueue(player);

        socket.eventHandlers['joinRoom'] = roomName => {
          if(privateSessions[roomName]) privateSessions[roomName].join(player);
          else {
            privateSessions[roomName] = new Session(io, mafia, roomName);
            privateSessions[roomName].join(player);
          }
          console.log(player.data.username+' joined room '+roomName)
          // mafia.joinPrivateRoom(roomName, player);
        }

        socket.eventHandlers['startGame'] = roomName => {
          if(privateSessions[roomName]) privateSessions[roomName].setup();
          //mafia.startPrivateGame(roomName);
        }

        socket.eventHandlers['phaserJoin'] = roomName => {
          console.log(player.data.username+' joined '+roomName);
          socket.join(roomName);
          socket.currentRoom = roomName;
          socket.broadcast.to(roomName).emit('event', {name:'phaserJoin', data:player.data})
        }

        socket.eventHandlers['phaserAction'] = action => {
          socket.broadcast.to(socket.currentRoom).emit('event', {name:'phaserAction', data:{
            player: player.data,
            action: action
          }})
        }

        socket.eventHandlers['phaserMe'] = data => {
          console.log('sending data to '+data.username);
          if(onlineUsers[data.to]) onlineUsers[data.to].socket.emitEvent('phaserMe', data);
        }

        socket.eventHandlers['phaserSpawn'] = position => {
          socket.broadcast.to(socket.currentRoom).emit('event', {name:'phaserSpawn', data:{
            player: player.data,
            position: position
          }})
        }

      } catch(err) {

        socket.disconnect('what the fuck are you doing')

      }
    })

    //var username = socket.handshake.query.username;

});
