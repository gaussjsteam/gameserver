var mongoose = require('mongoose');
var User = mongoose.model('User', {

  username: {
    type: String,
    required: true,
    unique: true
  },
  email: {
    type: String
  },
  password: {
    type: String
  },

  stats: [{
    game: String,
    gamesStarted: Number,
    gamesCompleted: Number,
    wins: Number
  }]

});

module.exports = User;
