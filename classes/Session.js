"use strict";
var _ = require('lodash');
var async = require('async');

class Session {

  constructor(io, game, roomName, players) {
    this.io = io;
    this.sessionId = roomName || '';
    this.players = players || [];
    this.game = game;
  }

  setup() {
    var playersReady = 0;
    //add each player to room and wait for them to send ready signal
    async.each(this.players, item => {
      item.socket.join(this.sessionId);
      item.socket.emit('event', {name:'gameReady'});
      item.socket.eventHandlers['ready'] = () => {
        playersReady += 1;
        //console.log(playersReady+' players ready');
        //start game when all players are ready
        if(playersReady == this.players.length) this.game.startGame(this.sessionId, _.clone(this.players));
        delete item.socket.eventHandlers['ready'];
      }
      item.socket.on('disconnect', () => {
        item.socket.broadcast.to(this.sessionId).emit('event', {name:'playerDisconnected', data:item.data});
      })
    })
  }

  join(player) {
    this.players.push(player);
    player.socket.join(this.sessionId);
    player.socket.broadcast.to(this.sessionId).emit('event', {name:'playerJoined', data:player.data});
    player.socket.on('disconnect', () => _.remove(this.players, item => item.data.username == player.data.username));
  }

  leave(player) {
    player.socket.broadcast.to(this.sessionId).emit('event', {name:'playerLeft', data:player.data});
    player.socket.leave(this.sessionId);
    _.remove(this.players, item => item.data.username == player.data.username);
  }

}

module.exports = Session;
