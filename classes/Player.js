"use strict";
var User = require("../models/User");

class Player {

  constructor(socket, username) {
    this.socket = socket;
    this.getUserData(username);
  }

  getUserData(username) {
    var that = this;
    if(username) {
      //find a player by requested username
      User.findOne({username:username}, function(err, user) {
        if(err) return console.log(err);
        if(!user) {
          //or create a new user with that username
          User.create({username:username}, function(err, user) {
            that.data = user;
          })
        } else {
          that.data = user;
        }
      })
    } else {
      //autogenerate username if none specified
      username = 'guest_'+new Date().getTime();
      that.data = new User({username: username});
    }
  }

}

module.exports = Player;
