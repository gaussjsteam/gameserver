"use strict";
var _ = require('lodash');
var async = require('async');
var Session = require('./Session');

//generic game
class Game {

  constructor(io, options) {
    this.io = io;
    options = options || {};
    this.queue = []; //public queue for matchmaking
    this.sessions = {};
    this.privateGames = {}; //list of private games
    this.minPlayers = options.minPlayers || 2; //minimum players allowed per game
    this.maxPlayers = options.maxPlayers || 2; //maximum players allowed per game
  }

  //join public queue
  joinQueue(player) {
    //add player to queue
    console.log('player joined queue', player.data.username);
    this.queue.push(player);
    //attempt to start the game if enough players in queue
    if(this.queue.length >= this.minPlayers) this.prepareGame();
  }

  //leave public queue
  leaveQueue(player) {
    //remove player from queue
    _.remove(this.queue, item => item.data.username == player.data.username);
  }

  //start game with enough players from public queue
  prepareGame() {
    //fetch players for game and remove them from queue
    var players = this.queue.slice(0, this.minPlayers);
    this.queue = _.difference(this.queue, players);
    //create new room
    var roomName = 'room_'+new Date().getTime();
    var session = new Session(this.io, this, roomName, players);
    session.setup();
  }

  //begin game
  startGame(roomName, players) {
    var playersInfo = _.map(players, 'data');
    async.each(players, item => {
      item.socket.eventHandlers['message'] = data => {
        console.log('got message', data);
        item.socket.broadcast.to(roomName).emit('event', {name: 'gotMessage', data: {sender: item.data, message: data}});
      }
    });
    //this.io.to(roomName).emit('gameStarted', playersInfo);
    this.game(roomName, players);
  }

  //game logic to implement in game class
  game(roomName, players) {
    this.endGame(roomName, players);
  }

  //finish game
  endGame(roomName, players, winners) {
    // async.each(players, function(item) {
    //   item.socket.emit('gameFinished');
    // })
  }

}

module.exports = Game;
