// Authentication service

var jwt = require('jsonwebtoken');
var secret = 'RxhtRegATM2YjlXXK81E8Zz4675S0j5k';

module.exports = {

  secret: secret,

  signToken: function(data) {
    return jwt.sign(data, secret);
  },

  verifyToken: function(token) {
    return jwt.verify(token, secret);
  },

  decodeToken: function(token) {
    return jwt.decode(token);
  }

};
