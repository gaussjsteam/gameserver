"use strict";
var mongoose = require('mongoose');
var random = require('mongoose-simple-random');
var async = require('async');
var Schema = mongoose.Schema;
var Game = require('../classes/Game');
var randomWords = require('random-words');
var _ = require('lodash');

var Word = mongoose.model(
  'Word',
  new Schema({
    word: String,
    value: Number,
    isSpecial: Boolean
  }).plugin(random)
);

class HitMe extends Game {

  game(roomName, players) {
    console.log('game started');
    var that = this;
    var words = []
    randomWords(30).forEach(function(word) {
      words.push({
        word: word,
        dmg: Math.pow(word.length, 1.5)*15,
        type: Math.random()>0.25?'damage':'mana'
      })
    })
    var i = 0;
    players.forEach(function(player) {
      player.socket.health = 1000;
      player.socket.mana = 0;
      player.socket.index = i;
      i += 1;
    });
    var playersInfo = _.map(players, 'data');
    this.io.to(roomName).emit('event', {name:'gameStarted', data:{players: playersInfo, words: words}});
    //this.io.to(roomName).emit('wordList', {words: words});
    async.each(players, item => {
      item.socket.eventHandlers['hit'] = data => {
        console.log('user called hit', data);
        var opponent = players[1 - item.socket.index];
        data.player = data.selfHit? item.data.username : opponent.data.username;
        var word = item.word?_.find(words, function(item) {return item.word = data.work}):false;
        if(word && word.type == 'mana') {
          data.player = item.data.username;
          this.io.to(roomName).emit('event', {name:'gotMana', data:data});
          item.socket.mana += Math.min(300, word.value);
          console.log(item.data.username+' mana', item.socket.mana);
        } else {
          data.player = data.selfHit? item.data.username : opponent.data.username;
          this.io.to(roomName).emit('event', {name:'gotHit', data:data});
          if(data.selfHit) {
            item.socket.health = Math.min(1000, item.socket.health -data.dmg);
            console.log(item.data.username+' health', item.socket.health);
          } else {
            opponent.socket.health = Math.min(1000, opponent.socket.health - data.dmg);
            console.log(opponent.data.username+' health', opponent.socket.health);
          }
          if(opponent.socket.health <= 0) {
            this.gameOver(roomName, item, opponent);
          } else if(item.socket.health <= 0) {
            this.gameOver(roomName, opponent, item);
          }
        }
      }
      item.socket.eventHandlers['useAbility'] = data => {
        console.log('user called ability', data);
        if(data.value) {
          if(item.socket.mana - data.value < 0) return;
          item.socket.mana -= data.value;
        }
        item.socket.broadcast.to(roomName).emit('event', {name:'usedAbility', data:data});
      }
      item.socket.on('disconnect', () => {
        var opponent = players[1 - item.socket.index];
        this.gameOver(roomName, opponent);
      })
      item.socket.eventHandlers['leaveGame'] = () => {
        console.log('player left', item.data.username);
        var opponent = players[1 - item.socket.index];
        item.socket.broadcast.to(roomName).emit('event', {name:'playerLeft', data:item.data});
        item.socket.leave(roomName);
        this.gameOver(roomName,opponent, item);
      }
    });
  }

  gameOver(roomName, winner, loser) {
    console.log(winner.data.username+' won');
    this.io.to(roomName).emit('event', {name:'gameOver', data:{winner: winner.data.username}});
    delete winner.socket.eventHandlers['hit'];
    delete winner.socket.eventHandlers['useAbility'];
    if(loser) {
      delete loser.socket.eventHandlers['hit'];
      delete loser.socket.eventHandlers['useAbility'];
    }
    //this.endGame(roomName, players);
  }

}

module.exports = HitMe;
