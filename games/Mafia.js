"use strict";
var async = require('async');
var _ = require('lodash');
var Game = require('../classes/Game');

class Mafia extends Game {

  constructor(io, options) {
    super(io, options);
    this.name = 'Mafia'
  }

  game(roomName, players) {
    console.log('game started');
    var that = this;
    var killer = Math.floor(Math.random()*players.length);
    for(var i=0; i<players.length; i++) {
      if(i==killer) players[i].socket.role = 'killer';
      else players[i].socket.role = 'citizen';
    }
    var playersReady = 0;
    async.each(players, function(player) {
      if(player.socket.role == 'killer') player.socket.emit('message', {text:'You are a killer', choices:['ok']});
      else player.socket.emit('message', {text:'You are a citizen', choices:['ok']});
      player.socket.once('choice', function(data) {
        playersReady += 1;
        if(playersReady == players.length) that.nightCycle(roomName, players);
      })
    });
  }

  nightCycle(roomName, players) {
    var that = this;
    var playersToKill = [];
    var playersReady = 0;
    this.io.to(roomName).emit('night');
    async.each(players, function(player) {
      if(player.socket.role == 'killer') {
        player.socket.emit('message', {text:'Select a citizen you want to kill', choices:_.map(_.map(players, 'data'), 'username')});
        player.socket.once('choice', function(data) {
          playersToKill.push(_.find(players, function(item) {
            return item.data.username == data;
          }));
          playersReady += 1;
          if(playersReady == players.length) that.dayCycle(roomName, players, playersToKill);
        })
      } else {
        player.socket.emit('message', {text:'It is night', choices:['sleep']});
        player.socket.once('choice', function(data) {
          playersReady += 1;
          if(playersReady == players.length) that.dayCycle(roomName, players, playersToKill);
        })
      }
    })
  }

  dayCycle(roomName, players, playersToKill) {
    var that = this;
    var deadPlayers;
    playersToKill.forEach(function(player) {
      deadPlayers = _.remove(players, function(item) {
        return item.data.username == player.data.username;
      });
    })
    deadPlayers.forEach(function(player) {
      player.socket.emit('message', {text:'You have been killed', choices:[]});
    })
    this.io.to(roomName).emit('day', {'deadPlayers': _.map(deadPlayers, 'data')});
    var votes = {};
    var playersReady = 0;
    async.each(players, function(player) {
      player.socket.emit('message', {text:_.map(_.map(deadPlayers, 'data'), 'username').join(', ')+' died. Vote for a citizen to lynch', choices:_.map(_.map(players, 'data'), 'username')});
      player.socket.once('choice', function(data) {
        if(votes[data]) votes[data] += 1;
        else votes[data] = 1;
        playersReady += 1;
        if(playersReady == players.length) that.voteCount(roomName, players, votes);
      })
    })
  }

  voteCount(roomName, players, votes) {
    var that = this;
    var maxVotes = _.max(_.values(votes));
    var playersToLynch = _.filter(_.keys(votes), function(i) {
      return votes[i] == maxVotes;
    });
    playersToLynch.forEach(function(playerName) {
      var player = _.find(players, function(item) {
        return item.data.username == playerName;
      })
      player.socket.emit('message', {text:'You have been lynched', choices:[]});
      _.remove(players, player);
    })
    var citizenCount = _.filter(players, function(item) {
      return item.socket.role == 'citizen';
    }).length;
    var killerCount = _.filter(players, function(item) {
      return item.socket.role == 'killer';
    }).length;
    if(killerCount >= citizenCount) {
      this.io.to(roomName).emit('gameOver', {text: 'The mafia won'});
      this.endGame(roomName, players);
    } else if(killerCount == 0) {
      this.io.to(roomName).emit('gameOver', {text: 'The citizens won'});
      this.endGame(roomName, players);
    } else {
      that.nightCycle(roomName, players);
    }
  }

}

module.exports = Mafia;
